from random import randint
from collections import OrderedDict

data = {randint(0,10): v for v in "cslekdw"}
# OrderedDict remembers the order entries were added
d = OrderedDict()
for k in data.keys():
    print(k)
    d[k] = data[k]