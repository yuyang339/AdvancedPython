# When we access an element of a tuple, we have to use index. 
# It means we have to remember the position of an attribute.
# This is not convenient.
# It would be better if we can access an element by a name.
# namedtuple is designed to address this issue.

from collections import namedtuple

Student = namedtuple("Student", ["name", "age", "sex"])

s1 = Student("jo", 18, "male")
s1.name
s1.age
s1.sex