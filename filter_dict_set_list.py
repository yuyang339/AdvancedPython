from random import randin

#############
# for dict, we can use dict comprehension to filter a dict
#############
data = {x: randint(-10, 10) for x in "ABCDEFG"}
filtered = {k: v for k, v in data.items() if v > 0}

#############
# for set, we can use set comprehension to filter a set
#############
data = [randint(-10, 10) for _ in range(20)]
data = set(data)
filtered = {x for x in data if x > 0}

#############
# for list, we can use filter or list comprehension to filter a list
# list comprehension is faster than filter
#############

# list: use filter
data = [randin(-10, 10) for _ in range(10)]
filtered = filter(lambda x: x > 0, data)


# list: use list comprehension
filtered = [x for x in data if x > 0]