from collections import Counter
import re

# given a txt file, count the most frequently words

txt = "While William Shakespeare’s reputation is based primarily on his plays, he became famous first as a poet. With the partial exception of the Sonnets (1609), quarried since the early 19th century for autobiographical secrets allegedly encoded in them, the nondramatic writings have traditionally been pushed to the margins of the Shakespeare industry. Yet the study of his nondramatic poetry can illuminate Shakespeare’s activities as a poet emphatically of his own age, especially in the period of extraordinary literary ferment in the last ten or twelve years of the reign of Queen Elizabeth."

c = Counter(re.split('\W+', txt))
c.most_common(10)
