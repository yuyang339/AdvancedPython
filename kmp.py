def KMP(t, p, next):
    i = 0 
    j = 0

    while i < len(t) and j < len(p):
        if j == -1 or t[i] == p[j]: 
            i += 1
            j += 1
        else:
            j = next[j]
    if j == len(p):
       return i - j
    else:
       return -1

# calculate partial matching table
# compare the p0=p[0:-1] and p1=p[1:-1]
# if p0[i] == p1[j], then ++i, ++j, next[i]=j meaning p0[i-j:i] matches p1[0:j]

def getNext(p):
    next = [0]*len(p)
    next[0] = -1
    i = 0
    j = -1
    while i < len(p)-1:
        if j == -1 or p[i] == p[j]:
            i += 1
            j += 1
            next[i] = j
        else:
            j = next[j]
    return next
t = "aababababcab"
p = "fhgfjgfgjh"
next = getNext(p)
print(KMP(t, p, next))