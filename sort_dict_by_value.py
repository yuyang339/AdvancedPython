from random import randint
data = {k: randint(60, 100) for k in "ABCDEFG"}

#################
# use items() or zip method to form a list of tuple
# use sorted() to sort the dict by value
#################

# sorted method only sorts the dict by key. 
# it only returns the keys of the dict
# sorted(data)
c1 = [(v, k) for k, v in data.items()]
c2 = [(v, k) for k, v in zip(data.values(), data.keys())]
sorted(c1)
sorted(c2)

###################
# use the key argument in the sorted method
###################
# items() return a list of tuple.
# the first element of a tuple is key and the second element of a tuple is value
sorted(data.items(), key=lambda x: x[1])
